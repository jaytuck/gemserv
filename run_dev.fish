#!/usr/bin/env fish

set -x GEM_SQLITE_DATABASE 'dev.db'
source (pyenv init - fish | psub)
pyenv activate gem
python server.py

