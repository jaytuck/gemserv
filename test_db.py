import pytest
import sqlite3
from db import *
from sqlalchemy.orm import Session



@pytest.fixture
def dummy_pages():
    p1 = Page(id=1, body='Test page 1', url='page1', pagetype='page')
    p2 = Page(id=2, body='Test page 2', url='page2', pagetype='page')
    p3 = Page(id=3, body='Test pag', url='page3', pagetype='draft')
    return (p1, p2, p3)


@pytest.fixture
def dummy_db(dummy_pages):
    engine = getengine('sqlite://')
    init_db(engine) # set up the DB tables

    db_session = Session(engine)
    for page in dummy_pages:
        db_session.add(page)
        db_session.commit()

    return db_session


def test_render_page(dummy_db, dummy_pages):
    assert render_page(dummy_db, 1) == dummy_pages[0].body
    assert render_page(dummy_db, 2) == dummy_pages[1].body
    assert render_page(dummy_db, 'page1') == dummy_pages[0].body
    assert render_page(dummy_db, 'page2') == dummy_pages[1].body
    with pytest.raises(PageNotFoundError):
        assert render_page(dummy_db, 3)
    assert render_page(dummy_db, 3, allow_drafts=True) == dummy_pages[2].body
    with pytest.raises(PageNotFoundError):
        assert render_page(dummy_db, 99)
    with pytest.raises(PageNotFoundError):
        assert render_page(dummy_db, 'badurl')


def test_create_page(dummy_db):
    assert type(create_page(dummy_db, Page(body="Test Page 4", url='page4', pagetype='page'))) is int


def test_tag_page():
    pass

def test_all_tags_page():
    pass


