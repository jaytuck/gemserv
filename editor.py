import os
import db
import tempfile
import subprocess
from pathlib import Path
from pprint import pprint

db.init_db()  # init the db if not yet done

def edit_page(existing_text = ''):
    f = tempfile.mkstemp()
    scratchfile = Path(f[1])
    scratchfile.write_text(existing_text)
    editor = os.environ["EDITOR"] if "EDITOR" in os.environ else "vim"
    p = subprocess.run([editor, str(scratchfile)])
    new_text = scratchfile.read_text()
    os.unlink(f[1])  # clean up the file
    return new_text


while True:
    db_session = db.getsession()
    print(f'''What do you want to do?
1. new page
2. edit page
3/q quit''')
    command = input("Command: ")
    pprint(f'got command: {command}')
    if command.lower() in ('q', '3'):
        exit(0)
    elif command == '1':
        slug = input("What page slug do you want to create? ")
        try:
            existing_page = db.get_page(db_session, slug)
            print(f'Page with slug {slug} already exists!')
            continue
        except db.PageNotFoundError:
            pass  # page doesn't exist
        
        new_text = edit_page()
        print(f'got:\n{new_text}')
        saveasdraft = input('save as ([d]raft/[p]age/[c]ancel)? ').lower()
        if saveasdraft == 'p':
            page = db.Page(body=new_text, url=slug, pagetype='page')
        elif saveasdraft == 'c':
            continue
        else: # or in other words saveasdraft == 'd':
            page = db.Page(body=new_text, url=slug, pagetype='draft')

        db.create_page(db_session, page)
    elif command == '2':
        pages = db.get_pages(db_session)
        for page in pages:
            _body = page.body[0:50].replace('\n','\\n')
            print(f'id={page.id} slug={page.url} snippet={_body}')
        slug = input("What page slug do you want to edit? ")
        try:
            try:
                pageid = int(slug)
                existing_page = db.get_page(db_session, pageid)
            except ValueError:
                existing_page = db.get_page(db_session, slug)
        except db.PageNotFoundError:
            print(f'Page with slug {slug} doesn\'t exist!')
            continue
        
        new_text = edit_page(existing_page.body)
        print(f'got:\n{new_text}')
        existing_page.body = new_text
        saveasdraft = input('save as ([d]raft/[p]age/[c]ancel)? ').lower()
        if saveasdraft == 'p':
            existing_page.type = 'page'
        elif saveasdraft == 'c':
            continue
        else: # or in other words saveasdraft == 'd':
            existing_page.type = 'draft'
            

        # Tags
        if existing_page.tags:
            print(f'current page tags: {",".join(map(lambda tag: tag.name, existing_page.tags))}')
        else:
            print(f'no current page tags')

        tags = input('provide a comma-seperated list of tags for the page: ').lower()
        tags = list(map(lambda x: x.strip(), tags.split(',')))
        print(f'adding tags: {",".join(tags)}')
        for tag in tags:
            t = db.get_tag(db_session, name=tag, create=True)
            pprint(t)
            existing_page.tags.append(t)
            pprint(existing_page.tags)
        db.update_page(db_session, existing_page)


    # Before relooping, make sure to close the session, which will also rollback any changes
    db_session.close()

