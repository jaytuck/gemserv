# Database for gemserv.py

import os
import sqlite3
import uuid
#from pydantic import BaseModel, ValidationError, validator
#from enum import Enum
from sqlalchemy import (
    MetaData, create_engine,
    Table, Column, func,
    Integer, String, Date, Boolean, DateTime, ForeignKey,
    select,
)
from sqlalchemy.orm import declarative_base, Session, relationship
from sqlalchemy.exc import NoResultFound


Base = declarative_base()

pagetaglink_table = Table('pagetaglink', Base.metadata,
    Column('page_id', Integer, ForeignKey('page.id')),
    Column('tag_id', Integer, ForeignKey('tag.id')),
)

class Page(Base):
    __tablename__ = 'page'
    id = Column(Integer, primary_key=True)
    url = Column(String(1024), nullable=False, unique=True)
    body = Column(String)
    pagetype = Column(String, nullable=False, default='page')
    feeduuid = Column(String(35), nullable=False, unique=True, default=lambda: str(uuid.uuid4()))
    creationtimestamp = Column(DateTime, nullable=False, server_default=func.datetime())
    modifiedtimestamp = Column(DateTime, nullable=False, server_default=func.datetime())
    tags = relationship("Tag", secondary=pagetaglink_table, back_populates="pages")

    def __repr__(self):
        return(f'<Page id={self.id}, url={self.url[0:10]}...>')

class Tag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)
    pages = relationship("Page", secondary=pagetaglink_table, back_populates="tags")



def getengine(database_path=f"sqlite:///{os.environ['GEM_SQLITE_DATABASE']}"):
    engine = create_engine(database_path, future=True)
    return engine

def getsession():
    return Session(getengine())

class PageNotFoundError(Exception):
    pass
class InvalidPageUrlError(Exception):
    pass


def init_db(engine=None):
    if not engine:
        engine = getengine()
    Base.metadata.create_all(engine)


def get_page(db_session, id_or_url):
    try:
        if type(id_or_url) is int:
            page = db_session.execute(
                select(Page).where(Page.id == id_or_url)
            ).scalar_one()
        else:
            page = db_session.execute(
                select(Page).where(Page.url == id_or_url)
            ).scalar_one()
        return page
    except NoResultFound:
        raise PageNotFoundError(f'No page with id or url: {id_or_url}')


def render_page(dbconn, id_or_url, allow_drafts = False):
    page = get_page(dbconn, id_or_url)
    if not allow_drafts and page.pagetype == 'draft':
        raise PageNotFoundError(f'page {id_or_url} is a draft')
    return page.body

def validate_url(url):
    if ' ' in url:
        raise InvalidPageUrlError(f'space character not allowed in page url: [{url}]')
    if '/' in url:
        raise InvalidPageUrlError(f'"/" character not allowed in page url: [{url}]')
    if len(url) >= 50:
        raise InvalidPageUrlError(f'page url is too long! {len(url)}')


def create_page(db_session, page):
    validate_url(page.url)
    db_session.add(page)
    db_session.commit()
    return page.id


def update_page(db_session, page):
    validate_url(page.url)
    db_session.add(page)
    db_session.commit()


def get_pages(db_session):
    pages = db_session.execute(select(Page)).scalars()
    return pages


def get_tag(db_session, name, create=False):
    try:
        return db_session.execute(select(Tag).where(Tag.name == name)).scalar_one()
    except NoResultFound:
        if create:
            return Tag(name=name)
        else:
            raise


if __name__ == "__main__":
    from pprint import pprint as pp
    init_db()
    engine = getengine()
    session = Session(engine)


