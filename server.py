#!/usr/bin/env python3

import ssl
import trio
import os
import urllib
import db
from db import PageNotFoundError, render_page

# TODO:  trio.CapacityLimiter

class RequestTooLongError(Exception):
    pass
class InvalidRequestError(Exception):
    pass


async def receive_request(conn: trio.SSLStream) -> bytes:
    with trio.fail_after(5):
        request = b''
        while (data := await conn.receive_some(1026)) != b'':
            request += data
            if b'\r\n' in request:
                break # We have found the end of the request, move on
            if len(request) > 1026:
                raise RequestTooLongError()
    # Confirm there's no data after the request
    if request[-2:] != b'\r\n':
        raise InvalidRequestError()
    
    # Decode the request to unicode:
    unicode_request = request[:-2].decode() # Strip of the ending in the request
    
    parsed_url = urllib.parse.urlparse(unicode_request)
    return parsed_url 


async def return_response(conn: trio.SSLStream, status: int, meta: str = '', body: str = ''):
    await conn.send_all(f'{status} {meta}\r\n{body}'.encode())
async def return_good_response(conn: trio.SSLStream, body: str):
    await return_response(conn, 20, 'text/plain', body)


async def serve_request(conn: trio.SSLStream):
    try:
        with trio.fail_after(60):
            # maximum time a whole request can take is 60s
            print(f'received connection from {conn.transport_stream.socket.getpeername()}')

            # Receive the request
            request = await receive_request(conn)
            
            # Process the request
            if request.path == '/':
                await return_response(conn, 20, 'text/plain', f'you made the request: {request}')
            else:
                page_slug = request.path[1:]
            db_session = db.getsession()
            try:
                await return_good_response(conn, render_page(db_session, page_slug))
            except PageNotFoundError:
                await return_response(conn, 51, 'Page not found')



            print(f'got request: {request}')




    except trio.TooSlowError:
        print(f'request failed - whole request took too long')
    except RequestTooLongError:
        await return_response(conn, 59, f'BAD REQUEST - request string too long')

    

async def main():
    # Set up listener
    tlsctx = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
    tlsctx.load_cert_chain(certfile='cert.pem')
    # Set up DB
    db.init_db() # make sure the DB is set up

    listener = await trio.serve_ssl_over_tcp(handler=serve_request, port=8001, ssl_context=tlsctx)



    # Listen for connections
    while True:
        await trio.sleep(5)
        print('hi')


trio.run(main)
